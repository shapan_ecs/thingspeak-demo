### ThingSpeak API Demo

####Tools required:
* python 2.x.x
* node.js
* python  psutil module
* node connect module

### How to operate

```
cd upload
# upload data to thingspeak
python upload.py

#start local server
cd ../local-server
node server.js
```
when server starts, type http://localhost:3000 in your browser.


